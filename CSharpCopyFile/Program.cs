﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CSharpCopyFile
{
    class Program
    {
        static void Main(string[] args)
        {
            var srcFileName = "src.txt";
            var dstFileName = "dst.txt";

            Console.WriteLine($"dstFile exists={File.Exists(dstFileName)}");

            var streamWriter = File.CreateText(srcFileName);
            streamWriter.Close();

            File.Copy(srcFileName, dstFileName);
            Console.WriteLine($"dstFile exists={File.Exists(dstFileName)}");

            File.Delete(srcFileName);
            File.Delete(dstFileName);
        }
    }
}
